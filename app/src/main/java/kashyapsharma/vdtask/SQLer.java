package kashyapsharma.vdtask;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kashy on 1/12/2018.
 */

public class SQLer extends SQLiteOpenHelper {


    private static final String DB_NAME = "Item_Details";
    private static final int DB_VERSION = 1;

    public SQLer(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE ItemTable (" +
                "tbl_product_id TEXT NOT NULL PRIMARY KEY," +
                "category_id TEXT NOT NULL," +
                "product_name TEXT," +
                "description TEXT," +
                "image TEXT)");
        db.execSQL("CREATE TABLE ItemDetail (" +
                "tbl_product_id TEXT NOT NULL PRIMARY KEY," +
                "category_id TEXT NOT NULL," +
                "product_name TEXT," +
                "description TEXT," +
                "image TEXT," +
                "tbl_stock_id TEXT," +
                "product_id TEXT," +
                "stock TEXT)");
    }

    //CRUD

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS ItemTable");
        db.execSQL("DROP TABLE IF EXISTS ItemDetail");
        onCreate(db);
    }
    public boolean isExistItem(String tbl_product_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + "ItemDetail" + " WHERE " + "tbl_product_id" + " = ?",
                    new String[]{tbl_product_id});
            if (cursor != null && cursor.getCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    public void addInItemDetail(String tbl_product_id, String category_id, String product_name, String description, String image, String tbl_stock_id, String product_id, String stock) {
        SQLiteDatabase db = getWritableDatabase(); // opens the database connection
        try {
            ContentValues values = new ContentValues();
            values.put("tbl_product_id" , tbl_product_id);
            values.put("category_id" , category_id);
            values.put("product_name" , product_name);
            values.put("description" , description);
            values.put("image" , image);
            values.put("tbl_stock_id" , tbl_stock_id);
            values.put("product_id" , product_id);
            values.put("stock" , stock);

            if (isExistItem(tbl_product_id)== true) {
                db.update("ItemDetail", values, "tbl_product_id" + " = ?",
                        new String[]{tbl_product_id});
            } else {
                db.insert ("ItemDetail", null, values);
            }


        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            db.close();
        }
    }


    public List<Detailed> getDetailed(String tbl_product_id) {

        List<Detailed> data = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + "ItemDetail" + " WHERE " + "tbl_product_id" + " = ?",
                new String[]{tbl_product_id});

        try {
            if (cursor.moveToFirst()) {
                do {
                    Detailed det = new Detailed();
                    det.tbl_product_id = cursor.getString(cursor.getColumnIndex("tbl_product_id"));
                    det.category_id = cursor.getString(cursor.getColumnIndex("category_id"));
                    det.product_name = cursor.getString(cursor.getColumnIndex("product_name"));
                    det.description = cursor.getString(cursor.getColumnIndex("description"));
                    det.image = cursor.getString(cursor.getColumnIndex("image"));
                    det.tbl_stock_id = cursor.getString(cursor.getColumnIndex("tbl_stock_id"));
                    det.product_id = cursor.getString(cursor.getColumnIndex("product_id"));
                    det.stock = cursor.getString(cursor.getColumnIndex("stock"));

                    data.add(det);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return data;

    }
    public boolean isExist(String tbl_product_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + "ItemTable" + " WHERE " + "tbl_product_id" + " = ?",
                    new String[]{tbl_product_id});


            if (cursor != null && cursor.getCount() > 0) {

                return true;
            } else {

                return false;
            }
        }
        return false;
    }
    public void addInItemTable(String tbl_product_id, String category_id, String product_name, String description, String image) {
        SQLiteDatabase db = getWritableDatabase(); // opens the database connection
        try {
            ContentValues values = new ContentValues();
            values.put("tbl_product_id" , tbl_product_id);
            values.put("category_id" , category_id);
            values.put("product_name" , product_name);
            values.put("description" , description);
            values.put("image" , image);

            if (isExist(tbl_product_id)== true) {
                db.update("ItemTable", values, "tbl_product_id" + " = ?",
                        new String[]{tbl_product_id});
            } else {
                db.insert ("ItemTable", null, values);
            }


        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            db.close();
        }
    }
//    public List<Detailed> getDetailed(String tbl_product_id) {
//
//        List<Detailed> data = new ArrayList<>();
//        SQLiteDatabase db = getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + "ItemDetail" + " WHERE " + "tbl_product_id" + " = ?",
//                new String[]{tbl_product_id});
//
//        try {
//            if (cursor.moveToFirst()) {
//                do {
//                    Detailed det = new Detailed();
//                    det.tbl_product_id = cursor.getString(cursor.getColumnIndex("tbl_product_id"));
//                    det.category_id = cursor.getString(cursor.getColumnIndex("category_id"));
//                    det.product_name = cursor.getString(cursor.getColumnIndex("product_name"));
//                    det.description = cursor.getString(cursor.getColumnIndex("description"));
//                    det.image = cursor.getString(cursor.getColumnIndex("image"));
//                    det.tbl_stock_id = cursor.getString(cursor.getColumnIndex("tbl_stock_id"));
//                    det.product_id = cursor.getString(cursor.getColumnIndex("product_id"));
//                    det.stock = cursor.getString(cursor.getColumnIndex("stock"));
//
//                    data.add(det);
//
//                } while (cursor.moveToNext());
//            }
//        } catch (Exception e) {
//
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//        }
//
//        return data;
//
//    }
//    public boolean isExist(String tbl_product_id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        if (db != null) {
//            Cursor cursor = db.rawQuery("SELECT * FROM " + "ItemTable" + " WHERE " + "tbl_product_id" + " = ?",
//                    new String[]{tbl_product_id});
//
//
//            if (cursor != null && cursor.getCount() > 0) {
//
//                return true;
//            } else {
//
//                return false;
//            }
//        }
//        return false;
//    }
//    public void addInItemTable(String tbl_product_id, String category_id, String product_name, String description, String image) {
//        SQLiteDatabase db = getWritableDatabase(); // opens the database connection
//        try {
//            ContentValues values = new ContentValues();
//            values.put("tbl_product_id" , tbl_product_id);
//            values.put("category_id" , category_id);
//            values.put("product_name" , product_name);
//            values.put("description" , description);
//            values.put("image" , image);
//
//            if (isExist(tbl_product_id)== true) {
//                db.update("ItemTable", values, "tbl_product_id" + " = ?",
//                        new String[]{tbl_product_id});
//            } else {
//                db.insert ("ItemTable", null, values);
//            }
//
//
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
//        finally {
//            db.close();
//        }
//    }

    public List<Items> getEyeWear() {

        List<Items> data = new ArrayList<>();
        String DETAIL_SELECT_QUERY = "SELECT * FROM ItemTable where category_id = 1";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(DETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    Items allItems = new Items();
                    allItems.tbl_product_id = cursor.getString(cursor.getColumnIndex("tbl_product_id"));
                    allItems.category_id = cursor.getString(cursor.getColumnIndex("category_id"));
                    allItems.product_name = cursor.getString(cursor.getColumnIndex("product_name"));
                    allItems.description = cursor.getString(cursor.getColumnIndex("description"));
                    allItems.image = cursor.getString(cursor.getColumnIndex("image"));

                    data.add(allItems);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return data;

    }
    public List<Items> getShoe() {

        List<Items> data = new ArrayList<>();
        String DETAIL_SELECT_QUERY = "SELECT * FROM ItemTable where category_id = 2";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(DETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    Items allItems = new Items();
                    allItems.tbl_product_id = cursor.getString(cursor.getColumnIndex("tbl_product_id"));
                    allItems.category_id = cursor.getString(cursor.getColumnIndex("category_id"));
                    allItems.product_name = cursor.getString(cursor.getColumnIndex("product_name"));
                    allItems.description = cursor.getString(cursor.getColumnIndex("description"));
                    allItems.image = cursor.getString(cursor.getColumnIndex("image"));

                    data.add(allItems);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return data;

    }

    public List<Items> getKurti() {

        List<Items> data = new ArrayList<>();
        String DETAIL_SELECT_QUERY = "SELECT * FROM ItemTable where category_id = 3";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(DETAIL_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    Items allItems = new Items();
                    allItems.tbl_product_id = cursor.getString(cursor.getColumnIndex("tbl_product_id"));
                    allItems.category_id = cursor.getString(cursor.getColumnIndex("category_id"));
                    allItems.product_name = cursor.getString(cursor.getColumnIndex("product_name"));
                    allItems.description = cursor.getString(cursor.getColumnIndex("description"));
                    allItems.image = cursor.getString(cursor.getColumnIndex("image"));

                    data.add(allItems);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return data;

    }



}
