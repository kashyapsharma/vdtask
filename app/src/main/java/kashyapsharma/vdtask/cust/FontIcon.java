package kashyapsharma.vdtask.cust;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import kashyapsharma.vdtask.R;

/**
 * Created by kashy on 1/11/2018.
 */

public class FontIcon extends TextView {


    public FontIcon(Context context) {
        super(context);
        if(isInEditMode())
        {
            return;
        }
        Typeface tf = FontCache.get("fonts/flat.ttf", context);
        if(tf != null) {
            this.setTypeface(tf);
        }
    }

    public FontIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf;
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.textfontstyle, 0, 0);
        String font_name = a.getString(R.styleable.textfontstyle_fontname);
        a.recycle();
        tf= FontCache.get("fonts/flat.ttf", context);
        if(tf != null) {
            this.setTypeface(tf);
        }
    }

    public FontIcon(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface tf = FontCache.get("fonts/flat.ttf", context);
        if(tf != null) {
            this.setTypeface(tf);

        }
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }

}
