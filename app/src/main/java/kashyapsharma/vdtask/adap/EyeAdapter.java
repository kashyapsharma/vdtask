package kashyapsharma.vdtask.adap;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kashyapsharma.vdtask.ActivityDetaled;
import kashyapsharma.vdtask.Items;
import kashyapsharma.vdtask.R;

/**
 * Created by kashy on 1/12/2018.
 */

public class EyeAdapter extends RecyclerView.Adapter<EyeAdapter.ViewHolder> {
    private List<Items> item;
    private Context context;

    public EyeAdapter(Context context, List<Items> item) {
        this.item = item;
        this.context = context;
    }



    @Override
    public EyeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_nig, viewGroup, false);
        return new EyeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EyeAdapter.ViewHolder viewHolder, int position) {
        viewHolder.name.setText(item.get(position).product_name);
        viewHolder.desc.setText(item.get(position).description);
        String url = item.get(position).image.replace("www.","http://www.");
        Glide.with(context)
                .load(url)
                .into(viewHolder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return this.item.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView thumbnail;
        TextView desc;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            name = (TextView) itemView.findViewById(R.id.name);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            desc = (TextView) itemView.findViewById(R.id.desc);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Intent i = new Intent(context, ActivityDetaled.class);
            i.putExtra("item_id", item.get(position).tbl_product_id);
            context.startActivity(i);
        }

    }

    public void notify(List<Items> list) {
        if (item != null) {
            item.clear();
            item.addAll(list);

        } else {
            item = list;
        }
        notifyDataSetChanged();
    }
}
