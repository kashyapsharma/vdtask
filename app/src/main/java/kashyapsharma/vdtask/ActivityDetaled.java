package kashyapsharma.vdtask;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ActivityDetaled extends AppCompatActivity {

    private String item_id;
    private ImageView imageview;
    private ImageView back;
    private TextView desc;
    private TextView button,button1;
    public List<Detailed> data;
    private SQLer db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detaled);
        db = new SQLer(this);
        Intent intent = getIntent();
        item_id  = intent.getStringExtra("item_id");
        detailedJson(item_id);
    }

    private void detailedJson(String item_id) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://www.virtualdusk.com/vd/product_details.php?token=virtualdusk&product_id="+item_id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response.getJSONObject("products");
                            String tbl_product_id = jsonObject.getString("tbl_product_id");
                            String category_id = jsonObject.getString("category_id");
                            String product_name = jsonObject.getString("product name");
                            String description = jsonObject.getString("description");
                            String image = jsonObject.getString("image");
                            String tbl_stock_id = jsonObject.getString("tbl_stock_id");
                            String product_id = jsonObject.getString("product_id");
                            String stock = jsonObject.getString("stock");
                            imageview = (ImageView) findViewById(R.id.image1);
                            back = (ImageView) findViewById(R.id.back);
                            desc = (TextView) findViewById(R.id.desc);
                            button = (TextView) findViewById(R.id.button);
                            button1 = (TextView) findViewById(R.id.button1);
                            back.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    onBackPressed();
                                }
                            });
                            db.addInItemDetail(tbl_product_id, category_id, product_name, description, image, tbl_stock_id, product_id, stock);
                            // Fetching Product details from sqlite
                            data = db.getDetailed(tbl_product_id);
                            // Displaying the Product details on the screen
                            desc.setText(data.get(0).product_name+" "+data.get(0).description);
                            int stocke = Integer.parseInt(data.get(0).stock);
                            if (stocke == 0){
                                button.setBackgroundColor(Color.parseColor("#AE0202"));
                                button.setText("Out of stock");
                                button1.setVisibility(View.GONE);
                            } else {
                                if(stocke<=2){
                                    button.setText("Only "+stocke+" left in stock");
                                }else{
                                    button.setText(stocke+" left in stock");
                                }
                                button1.setVisibility(View.VISIBLE);
                                button.setBackgroundColor(Color.parseColor("#125688"));

                                button1.setText("Buy");
                            }

                            String url =  data.get(0).image.replace("www.","http://www.");
                            Log.e("hello",url);

                            Glide.with(ActivityDetaled.this)
                                    .load(url)

                                    .into(imageview);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }                       }
                },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("", "Error: " + error.getMessage());
            }
        });


        AppController.getInstance().addToRequestQueue(request);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
