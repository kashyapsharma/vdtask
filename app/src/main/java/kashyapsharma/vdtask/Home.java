package kashyapsharma.vdtask;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.List;

import kashyapsharma.vdtask.cust.FontIcon;
import kashyapsharma.vdtask.frag.EyeFragment;
import kashyapsharma.vdtask.frag.KurtiFragment;
import kashyapsharma.vdtask.frag.ShoeFragment;

public class Home extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context=this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons(context);
    }
    private void setupTabIcons(Context context) {

        FontIcon tabOne = (FontIcon) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(R.string.chashma);
        tabLayout.getTabAt(0).setCustomView(tabOne);
        FontIcon tabTwo = (FontIcon) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getApplicationContext().getResources().getString(R.string.shoe));

        tabLayout.getTabAt(1).setCustomView(tabTwo);

        FontIcon tabThree = (FontIcon) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(context.getResources().getString(R.string.kurti));
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new EyeFragment(), "ONE");
        adapter.addFrag(new ShoeFragment(), "TWO");
        adapter.addFrag(new KurtiFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
