package kashyapsharma.vdtask.frag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kashyapsharma.vdtask.AppController;
import kashyapsharma.vdtask.Items;
import kashyapsharma.vdtask.R;
import kashyapsharma.vdtask.SQLer;
import kashyapsharma.vdtask.adap.ShoeAdapter;


public class ShoeFragment extends Fragment {

    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;

    private ShoeAdapter fa;
    private SQLer db;
    public List<Items> data;
    public ShoeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_first, container, false);

        initView(rootView);
        getData();
        return rootView;
    }

    private void initView(View rootView) {
        db = new SQLer(getActivity());
        layoutManager = new GridLayoutManager(getContext(),1);

        recyclerView=(RecyclerView)rootView.findViewById(R.id.recycler);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(layoutManager);
        fa = new ShoeAdapter(getActivity(), db.getShoe());
        recyclerView.setAdapter(fa);
        getData();
       // text.setText(getResources().getString(R.string.shoe));
    }
    private void getData(){

        Log.e("TAG","sa");

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, "http://www.virtualdusk.com/vd/products.php?token=virtualdusk&category_id=2", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("TAG", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("products");
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                String tbl_product_id = jsonObject.getString("tbl_product_id");
                                String category_id = jsonObject.getString("category_id");
                                String product_name = jsonObject.getString("product name");
                                String description = jsonObject.getString("description");
                                String image = jsonObject.getString("image");

                                db.addInItemTable(tbl_product_id, category_id, product_name, description, image);
                                data = db.getShoe();
                                fa.notify(data);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Tag", "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

}